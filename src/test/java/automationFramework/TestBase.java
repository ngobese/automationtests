package automationFramework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import java.util.concurrent.TimeUnit;

public class TestBase {
    public static WebDriver driver = null;

    public TestBase() {

    }

    @BeforeSuite
    public void initialize() {
        System.setProperty("webdriver.chrome.driver", ".idea//chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10L, TimeUnit.SECONDS);
        driver.get("http://bluelink.investec.co.za");
    }

    @AfterSuite
    public void TeardownTest() {
        driver.quit();
    }
}
