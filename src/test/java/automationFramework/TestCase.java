package automationFramework;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pageObjects.pageObjectsForex.*;
import pageObjects.pageObjectsHomePage.*;
import pageObjects.pageObjectsOrders.*;
import pageObjects.pageObjectsShipments.*;
import pageObjects.pageObjectsSuppliers.*;
import pageObjects.payments.*;

public class TestCase extends TestBase {

    @BeforeClass
    public void setLoginPage() throws InterruptedException{
        loginPage _signin = PageFactory.initElements(driver, pageObjects.pageObjectsHomePage.loginPage.class);
        Thread.sleep(2000);
        _signin.loginActions( "demouser", "demouser");
    }

   @Test
    public void testHomePage() throws InterruptedException {
        homePage _home = PageFactory.initElements(driver, pageObjects.pageObjectsHomePage.homePage.class);
        Thread.sleep(2000);
        _home.verifyDashboard();
    }

    @Test
    public void testTrackingConsole() throws InterruptedException{
        Thread.sleep(2000);
        trackingConsolePage _tracking = PageFactory.initElements(driver, pageObjects.pageObjectsHomePage.trackingConsolePage.class);
        _tracking.trackingConsole();
    }

    @Test
    public void testFacilityOverview() throws InterruptedException{
        facilityOverviewPage _facility = PageFactory.initElements(driver, pageObjects.pageObjectsHomePage.facilityOverviewPage.class);
        _facility.verifyFacility();
    }

    @Test
    public void testInvoiceOverview() throws InterruptedException{
        invoiceOverview _invoice = PageFactory.initElements(driver, pageObjects.pageObjectsHomePage.invoiceOverview.class);
        _invoice.verifyInvoice();
    }

   @Test
    public void testOrderOverview() throws InterruptedException{
        ordersOverview _orders = PageFactory.initElements(driver, pageObjects.pageObjectsOrders.ordersOverview.class);
        _orders.verifyOrdersOverview();
    }

    @Test
    public void testActiveOrders() throws InterruptedException{
        activeOrdersPage _activeOrders = PageFactory.initElements(driver, pageObjects.pageObjectsOrders.activeOrdersPage.class);
        _activeOrders.verifyActiveOrders();
    }

    @Test
    public void testOrdersAlreadyDelivered() throws InterruptedException{
        ordersAlreadyDeliveredPage _ordersDelivered = PageFactory.initElements(driver, pageObjects.pageObjectsOrders.ordersAlreadyDeliveredPage.class);
        _ordersDelivered.verifyOrdersAlreadyDelivered();
    }

    @Test
    public void testOrderSearch() throws InterruptedException{
        orderSearchPage _orderSearch = PageFactory.initElements(driver, pageObjects.pageObjectsOrders.orderSearchPage.class);
        _orderSearch.searchForOrders("Dessuv Opfytvsoem Du., Ltd.", "65*92.5 - CLASSIC");
    }

    @Test
    public void testShipmentOverview() throws InterruptedException{
        shipmentOverviewPage _shipments = PageFactory.initElements(driver, pageObjects.pageObjectsShipments.shipmentOverviewPage.class);
        _shipments.verifyShipmentOverview();
    }

    @Test
    public void testShipmentCalculator() throws InterruptedException{
        shipmentCalculatorPage _shipmentCalc = PageFactory.initElements(driver, pageObjects.pageObjectsShipments.shipmentCalculatorPage.class);
        _shipmentCalc.calculateShipmentDates("30-Sep-2018");
    }

    @Test
    public void testShipmentCalendar() throws InterruptedException{
        shipmentCalendarPage _shipmentCalendar = PageFactory.initElements(driver, pageObjects.pageObjectsShipments.shipmentCalendarPage.class);
        _shipmentCalendar.verifyShipmentCalendar();
    }

    @Test
    public void testShipmentStatusReport() throws InterruptedException{
        shipmentStatusReportPage _statusReport = PageFactory.initElements(driver, shipmentStatusReportPage.class);
        _statusReport.verifyShipmentStatusReport();
    }

   @Test
    public void testShipmentSearch() throws InterruptedException{
        shipmentSearchPage _shipmentSearch = PageFactory.initElements(driver, pageObjects.pageObjectsShipments.shipmentSearchPage.class);
        _shipmentSearch.searchForShipments("Jeptum Vizvomi");
    }

    @Test
    public void testSupplierPerformance() throws InterruptedException{
        supplierPerformancePage _supplierPerform = PageFactory.initElements(driver, pageObjects.pageObjectsSuppliers.supplierPerformancePage.class);
        _supplierPerform.verifySupplierPerformance();
    }

    @Test
    public void testSupplierDetails() throws InterruptedException{
        supplierDetailsPage _supplierDetails = PageFactory.initElements(driver, pageObjects.pageObjectsSuppliers.supplierDetailsPage.class);
        _supplierDetails.verifySupplierDetails();
    }

    @Test
    public void testSupplierOverview() throws InterruptedException{
        supplierOverviewPage _supplier = PageFactory.initElements(driver, pageObjects.pageObjectsSuppliers.supplierOverviewPage.class);
        _supplier.verifySupplierOverview();
    }

   @Test
    public void testProductsOnOrder() throws InterruptedException{
        productsOnOrderPage _products = PageFactory.initElements(driver, pageObjects.pageObjectsSuppliers.productsOnOrderPage.class);
        _products.verifyProductsOoOrder("Jf");
    }

    @Test
    public void testProductSearch() throws InterruptedException{
        productSearchPage _productSearch = PageFactory.initElements(driver, productSearchPage.class);
       _productSearch.productSearch("Ck16ee2282284211");
    }

    @Test
    public void testProductsAlreadyDelivered() throws InterruptedException{
        productsAlreadyDeliveredPage _productsDelivered = PageFactory.initElements(driver, productsAlreadyDeliveredPage.class);
        _productsDelivered.productAlreadyDelivered("00222");
    }

    @Test
    public void testPaymentsOverview() throws InterruptedException{
        paymentsOverviewPage _payments = PageFactory.initElements(driver, paymentsOverviewPage.class);
        _payments.paymentsOverviewTests();
    }

    @Test
    public void testAllPayments() throws InterruptedException{
        allPaymentsPage _allPayments = PageFactory.initElements(driver, allPaymentsPage.class);
        _allPayments.allPaymentsTests("scheduled payment");
    }

    @Test
    public void testPaymentsCalender() throws InterruptedException{
        paymentsCalendarPage _paymentCalendar = PageFactory.initElements(driver, paymentsCalendarPage.class);
        _paymentCalendar.paymentsCalenderTests();
    }

    @Test
    public void testPaymentOfActiveOrders() throws InterruptedException{
        paymentsOfActiveOrdersPage _paymentActive = PageFactory.initElements(driver, paymentsOfActiveOrdersPage.class);
        _paymentActive.paymentsActiveOrders("Assign cover");
    }

   @Test
    public void testForexOverview() throws InterruptedException{
        forexOverviewPage _forex = PageFactory.initElements(driver, forexOverviewPage.class);
        _forex.forexOverviewTest("USD", "EUR");
    }

    @Test
    public void testAllForexOrders() throws InterruptedException{
        allForexOrdersPage _forexOrders = PageFactory.initElements(driver, allForexOrdersPage.class);
        _forexOrders.allForexOrdersTests("USD");
    }

    @Test
    public void testAllForexDeals() throws InterruptedException{
        allForexDealsPage _forexDeals = PageFactory.initElements(driver, allForexDealsPage.class);
        _forexDeals.allForexDealsTests("EUR");
    }

    @Test
    public void testQuoting() throws InterruptedException{
        quotingLitePage _quote = PageFactory.initElements(driver, quotingLitePage.class);
        _quote.quotingLiteTests("50000",
                "15",
                "China",
                "Shanghai",
                "Durban",
                "Johannesburg",
                "8000"
        );
    }

    @AfterClass
    public void logOut() throws InterruptedException{
        loginPage _signout = PageFactory.initElements(driver, loginPage.class);
        _signout.logoutActions();
    }
}