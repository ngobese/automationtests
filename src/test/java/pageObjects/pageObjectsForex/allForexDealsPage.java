package pageObjects.pageObjectsForex;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;
import utilities.pageElements;

public class allForexDealsPage {
    final WebDriver driver;

    @FindBy(how = How.XPATH, using = ".//*[@id='investecNavbarToggle']/div[3]")
    public WebElement menu;

    @FindBy(how = How.XPATH, using = "//a[@id='fxad_li']")
    public WebElement goToAllForexDeals;

    @FindBy(how = How.XPATH, using = "//input[@id='allForexDeals_search_field']")
    public WebElement searchForexDeals;

    @FindBy(how = How.XPATH, using = "//html//tr[2]/td[1]")
    public WebElement verifyForexDealsByCurrency;

    public allForexDealsPage(WebDriver driver){
        this.driver = driver;
    }

    public void allForexDealsTests(String searchEUR) throws InterruptedException {
        menu.click();
        Thread.sleep(1000);
        goToAllForexDeals.click();
        Thread.sleep(1000);
        searchForexDeals.sendKeys(searchEUR + Keys.ENTER);
        Assert.assertTrue(verifyForexDealsByCurrency.isDisplayed());
        Thread.sleep(1000);
        System.out.println("All forex deals tests passed");
    }


}
