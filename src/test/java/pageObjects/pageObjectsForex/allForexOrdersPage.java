package pageObjects.pageObjectsForex;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class allForexOrdersPage {
    final WebDriver driver;

    @FindBy(how = How.XPATH, using = ".//*[@id='investecNavbarToggle']/div[3]")
    public WebElement menu;

    @FindBy(how = How.XPATH, using = "//a[@id='fxao_li']")
    public WebElement goToAllForexOrders;

    @FindBy(how = How.XPATH, using = "//input[@id='allfo_search_field']")
    public WebElement searchByCurrency;

    @FindBy(how = How.XPATH, using = "//html//tr[2]/td[1]")
    public WebElement verifyForexOrdersExist;

    public allForexOrdersPage(WebDriver driver){
        this.driver = driver;
    }

    public void allForexOrdersTests(String searchCurrency) throws InterruptedException {
        menu.click();
        Thread.sleep(1000);
        goToAllForexOrders.click();
        Thread.sleep(1000);
        searchByCurrency.sendKeys(searchCurrency + Keys.ENTER);
        Assert.assertTrue(verifyForexOrdersExist.isDisplayed());
        Thread.sleep(1000);
        System.out.println("All forex orders tests passed");
    }
}
