package pageObjects.pageObjectsForex;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class forexOverviewPage {
    final WebDriver driver;

    @FindBy(how = How.XPATH, using = ".//*[@id='investecNavbarToggle']/div[3]")
    public WebElement menu;

    @FindBy(how = How.XPATH, using = "//a[@id='fxo_li']")
    public WebElement goToForexOverview;

    @FindBy(how = How.XPATH, using = "//input[@id='forexSummary_search_field']")
    public WebElement searchSummaryByCurrency;

    @FindBy(how = How.XPATH, using = "//span[@style='white-space:nowrap;'][contains(text(),'USD')]")
    public WebElement containsCurrency;

    @FindBy(how = How.XPATH, using = "//input[@id='forexAssigned_ig_toolbar_search_field']")
    public WebElement searchAssignedOrders;

    @FindBy(how = How.XPATH, using = "//html//div[@id='forexAssigned']//tr[1]/td[3]")
    public WebElement containsAssignedCurrency;

    @FindBy(how = How.XPATH, using = "//html//div[@id='R86106634545094372']//tr[1]/td[3]")
    public WebElement UnassignedDealsExists;

    public forexOverviewPage(WebDriver driver){
        this.driver = driver;
    }

    public void forexOverviewTest(String SearchUSD, String SearchEUR) throws InterruptedException {
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        menu.click();
        Thread.sleep(1000);
        goToForexOverview.click();
        Thread.sleep(1000);
        searchSummaryByCurrency.sendKeys(SearchUSD + Keys.ENTER);
        Assert.assertTrue(containsCurrency.isDisplayed());
        jse.executeScript("window.scrollBy(0,250)", "");
        searchAssignedOrders.sendKeys(SearchEUR + Keys.ENTER);
        Assert.assertTrue(containsAssignedCurrency.isDisplayed());
        jse.executeScript("window.scrollBy(0,250)", "");
        UnassignedDealsExists.isDisplayed();
        Thread.sleep(1000);
        System.out.println("Forex Overview tests passed");
    }
}
