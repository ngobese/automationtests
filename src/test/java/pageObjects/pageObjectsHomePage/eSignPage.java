package pageObjects.pageObjectsHomePage;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.security.Key;

public class eSignPage {

    final WebDriver driver;

    @FindBy(how = How.XPATH, using = ".//*[@id='investecNavbarToggle']/div[3]")
    public WebElement menu;

    @FindBy(how = How.XPATH, using = ".//*[@id='doc_hdg']/div/div[1]")
    public WebElement documentsMenuHeader;

    @FindBy(how = How.XPATH, using = ".//*[@id='mylist']")
    public WebElement goTo_eSign;

    @FindBy(how = How.XPATH, using = ".//*[@id='P14_ESIGN_PIN']")
    public WebElement eSignPinTxt;

    @FindBy(how = How.XPATH, using = ".//*[@id='R565085990027373_search_field']")
    public WebElement searchAwaitingSignatures;

    @FindBy(how = How.XPATH, using = ".//*[@id='565232009027374']/tbody/tr[2]/td[2]/a/span")
    public WebElement clickEnvelope;

    @FindBy(how = How.XPATH, using = ".//*[@id='B166321657983773']")
    public WebElement continueButton;

    @FindBy(how = How.XPATH, using = ".//*[@id='navigate-btn']")
    public WebElement startButton;

    @FindBy(how = How.XPATH, using = ".//*[@id='action-bar-btn-view-complete']")
    public WebElement closeButton;

    @FindBy(how = How.XPATH, using = ".//*[@id='otherActionsButton']")
    public WebElement otherActions;

    @FindBy(how = How.XPATH, using = "html/body/div[1]/div[1]/div[3]/div/div/div[4]/span[2]/div/div/ul/li[2]/button")
    public WebElement decline;

    @FindBy(how = How.XPATH, using = ".//*[@id='decline-warning']/div/div/div[3]/button[1]")
    public WebElement continueDecline;

    @FindBy(how = How.XPATH, using = ".//*[@id='reason-text']")
    public WebElement declineReasone;

    @FindBy(how = How.XPATH, using = ".//*[@id='decline-to-sign']/div/div/div[3]/button[1]")
    public WebElement declineToSign;

    @FindBy(how = How.XPATH, using = ".//*[@id='adopt-dialog']/div/div[3]/button[1]")
    public WebElement adoptSignature;

    @FindBy(how = How.XPATH, using = ".//*[@id='end-of-document-btn-finish']")
    public WebElement finishButton;

    public eSignPage(WebDriver driver){
        this.driver = driver;
    }

    public void findDocuments(String eSignPin, String paymentNumber) throws InterruptedException {

        menu.click();
        Thread.sleep(1500);
        documentsMenuHeader.isDisplayed();
        goTo_eSign.click();
        eSignPinTxt.sendKeys(eSignPin);
        continueButton.click();
        Thread.sleep(50000);
//        searchAwaitingSignatures.sendKeys(paymentNumber);
//        Thread.sleep(1000);
        clickEnvelope.click();
        Thread.sleep(5000);
    }

    public void signDocuments() throws InterruptedException {

        if (startButton.isDisplayed()){
            startButton.click();
            Thread.sleep(2000);
            driver.findElement(By.xpath("//*[contains(@id, 'tab-form-element')]")).click();
//            Thread.sleep(2000);
//            adoptSignature.click();
            Thread.sleep(2000);
            finishButton.click();
            Thread.sleep(5000);
        }
        else{
            closeButton.click();
            Thread.sleep(5000);
        }
    }

    public void declineToSign() throws InterruptedException {

        otherActions.click();
        Thread.sleep(2000);
        decline.click();
        System.out.println("Clicked the button");
        Thread.sleep(5000);
        continueDecline.click();
        Thread.sleep(2000);
        declineReasone.sendKeys("This is a test to decline documents automated");
        declineToSign.click();
        Thread.sleep(2000);
    }
}
