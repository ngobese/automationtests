package pageObjects.pageObjectsHomePage;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class eSignViewPage {
    final WebDriver driver;

    @FindBy(how = How.XPATH, using = ".//*[@id='investecNavbarToggle']/div[2]")
    public WebElement menu;

    @FindBy(how = How.XPATH, using = ".//*[@id='esn_v_li']")
    public WebElement goTo_eSignView;

    @FindBy(how = How.XPATH, using = ".//*[@id='esign_actions_report']/div[2]/div[2]")
    public WebElement verifyPendingVerifications;

    @FindBy(how = How.XPATH, using = ".//*[@id='esign_actions_report_search_field']")
    public WebElement searchDocPendingVerification;

    @FindBy(how = How.XPATH, using = "//*[contains(text(), 'Reject')]")
    public WebElement reject;

    @FindBy(how = How.XPATH, using = ".//*[@id='P69_REJECTION_REASON']")
    public WebElement rejectNotes;

    @FindBy(how = How.XPATH, using = ".//*[@id='B10641915062882658']")
    public WebElement submitRejection;

    @FindBy(how = How.LINK_TEXT, using = "Approve")
    public WebElement approveButton;

    @FindBy(how = How.XPATH, using = ".//*[@id='P69_AUTH_COMMENTS']")
    public WebElement authorisationComments;

    @FindBy(how = How.XPATH, using = ".//*[@id='P69_SIG_COMMENTS']")
    public WebElement signatoryComments;

    @FindBy(how = How.XPATH, using = ".//*[@id='B10595981389808198']")
    public WebElement sendForSigning;

    public eSignViewPage(WebDriver driver){
        this.driver = driver;
    }

    public void verify_eSignDocuments(String paymentNumber) throws InterruptedException {

        menu.click();
        Thread.sleep(1000);
        goTo_eSignView.click();
        Thread.sleep(2000);
        verifyPendingVerifications.isDisplayed();
        searchDocPendingVerification.sendKeys(paymentNumber);
        Thread.sleep(1000);


        //Approving the document

            approveButton.click();
            authorisationComments.sendKeys("This is a automated message for authorization letter (.) (.)");
            signatoryComments.sendKeys("Second automated message for signatory only (.) (.)");
            Thread.sleep(1000);
            sendForSigning.click();
            Thread.sleep(5000);


        //Reject the document

//        reject.click();
//        Thread.sleep(1000);
//        rejectNotes.sendKeys("Automated reject notes - Testing");
//        submitRejection.click();
//        Thread.sleep(2000);



    }
}
