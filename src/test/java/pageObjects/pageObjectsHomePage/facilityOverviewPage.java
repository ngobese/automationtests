package pageObjects.pageObjectsHomePage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class facilityOverviewPage {
    final WebDriver driver;

    @FindBy(how = How.XPATH, using = ".//*[@id='investecNavbarToggle']/div[3]")
    public WebElement menu;

    @FindBy(how = How.LINK_TEXT, using = "20,000,000.00")
    public WebElement verifyFacilitySummary;

    @FindBy(how = How.XPATH, using = "//div[@id='R1167025681649436843']//div[@class='t-Region-bodyWrap']//div[@class='t-Region-body']")
    public WebElement verifyCurrentExposure;


    @FindBy(how = How.XPATH, using = "//div[@id='R345114377959419694']//div[@class='t-Region-bodyWrap']//div[@class='t-Region-body']']")
    public WebElement verifyAverageFacilityUtilisation;

    @FindBy(how = How.XPATH, using = ".//*[@id='R828500843565593833']/div[1]/div[2]/span/button")
    public WebElement maximizeGraph;

    @FindBy(how = How.XPATH, using = ".//*[@id='LOG03']/div[1]/div[1]/ul/li[1]/a")
    public WebElement backToDashboarrd;

    @FindBy(how = How.XPATH, using = ".//*[@id='fac_li']")
    public WebElement clickFacilityOverview;

    public facilityOverviewPage(WebDriver driver){

        this.driver = driver;
    }

    public void verifyFacility() throws InterruptedException {
        menu.click();
        Thread.sleep(1000);
        clickFacilityOverview.click();
        Assert.assertTrue(driver.getPageSource().contains("Active facility"));
        Assert.assertTrue(driver.getPageSource().contains("20,000,000.00"));
        System.out.println("Facility Overview passed");
    }
}
