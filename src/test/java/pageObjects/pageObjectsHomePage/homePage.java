package pageObjects.pageObjectsHomePage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;
import org.testng.annotations.Factory;

public class homePage {

    final WebDriver driver;

    @FindBy(how = How.XPATH, using = "//html//div[@id='R23688692980423916']//div[@class='t-Report-tableWrap']//tr[1]/td[1]")
    public WebElement exchangeRatesWidget;

    @FindBy(how = How.XPATH, using = "//span[@class='t-BadgeList-label'][contains(text(),'EUR')]")
    public WebElement clickOrdersByCurrency;

    @FindBy(how = How.XPATH, using = "//td[@class='t-Report-cell'][contains(text(),'Number of orders CAD')]")
    public WebElement orderComparisonCurrentMonthWidget;

    @FindBy(how = How.XPATH, using = "//html//div[@id='R22645436436448007']//div[@class='t-Report-tableWrap']//tr[1]/td[1]")
    public WebElement forexSummary;

    @FindBy(how = How.XPATH, using = ".//*[@id='investecNavbarToggle']/div[3]")
    public WebElement menu;

    @FindBy(how = How.XPATH, using = "//a[@id='mydash']")
    public WebElement navigateToDashboard;

public homePage(WebDriver driver) {

    this.driver = driver;
    }

public void verifyDashboard() throws InterruptedException {
    menu.click();
    Thread.sleep(1000);
    navigateToDashboard.click();
    Thread.sleep(3000);
    Assert.assertTrue(driver.getTitle().contentEquals("Dashboard"));
    Assert.assertTrue(exchangeRatesWidget.isDisplayed());
    Assert.assertTrue(orderComparisonCurrentMonthWidget.isDisplayed());
    Assert.assertTrue(forexSummary.isDisplayed());
    clickOrdersByCurrency.click();
    Thread.sleep(2000);
    driver.navigate().back();
    Thread.sleep(3000);
    Assert.assertTrue(driver.getPageSource().contains("China"));
    Assert.assertTrue(driver.getPageSource().contains("Taiwan"));
    Assert.assertTrue(driver.getPageSource().contains("Korea, Republic of"));
    Assert.assertTrue(driver.getPageSource().contains("Turkey"));
    System.out.println("Home page dashboard widgets verified");
    }
}

