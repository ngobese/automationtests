package pageObjects.pageObjectsHomePage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class invoiceOverview {

    final WebDriver driver;

    @FindBy(how = How.XPATH, using = ".//*[@id='outstandingInvoices_worksheet_region']")
    public WebElement invoiceOverviewexists;

    @FindBy(how = How.XPATH, using = ".//*[@id='inv_li']")
    public WebElement clickInvoiceOverview;

    @FindBy(how = How.XPATH, using = ".//*[@id='investecNavbarToggle']/div[3]")
    public WebElement menu;

    public invoiceOverview(WebDriver driver){
        this.driver = driver;
    }

    public void verifyInvoice() throws InterruptedException {
        menu.click();
        Thread.sleep(1000);
        clickInvoiceOverview.click();
        Assert.assertTrue(invoiceOverviewexists.isDisplayed());
        Assert.assertTrue(driver.getPageSource().contains("30 Net Statement"));
        System.out.println("Invoice overview passed");
    }
}
