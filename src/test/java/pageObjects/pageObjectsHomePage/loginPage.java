package pageObjects.pageObjectsHomePage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class loginPage {
    final WebDriver driver;

    @FindBy(how = How.ID, using = "P101_USERNAME")
    public WebElement user;
    @FindBy(how = How.ID, using = "P101_PASSWORD")
    public WebElement pwd;
    @FindBy(how = How.XPATH, using = "//button[@id='B21984412680790373']//span[@class='t-Button-label']")
    public WebElement submit;

    @FindBy(how = How.XPATH, using = ".//*[@id='mylogin']")
    public WebElement logout;

    @FindBy(how = How.XPATH, using = "//button[@id='B26371036790741604']")
    public WebElement forgotPassword;

    @FindBy(how = How.XPATH, using = "//span[@class='ui-button-icon-primary ui-icon ui-icon-closethick']")
    public WebElement closeResetScreen;

    @FindBy(how = How.ID, using = "P72_EMAIL_ADDRESS")
    public WebElement emailField;

    @FindBy(how = How.CSS, using = "body.t-Dialog-page.t-Dialog--wizard.apex-top-nav.apex-icons-fontawesome:nth-child(2) div.t-Dialog:nth-child(8) div.t-Wizard.t-Wizard--modal div.t-Wizard-footer div.t-ButtonRegion.t-Form--floatLeft div.t-ButtonRegion-wrap div.t-ButtonRegion-col.t-ButtonRegion-col--content:nth-child(2) tbody:nth-child(1) tr:nth-child(1) td:nth-child(2) button.t-Button.t-Button--icon.t-Button--iconRight.t-Button--hot > span.t-Button-label:nth-child(2)")
    public WebElement continueButton;

    public loginPage(WebDriver driver){
        this.driver = driver;
    }

    public void passwordReset() throws InterruptedException {
        forgotPassword.click();
        Thread.sleep(3000);
       // emailField.sendKeys(new CharSequence[]{"lindo.ngobese@investec.com"});
        continueButton.click();
        closeResetScreen.click();
    }

    public void loginActions( String _username, String password) throws InterruptedException {
//        user.click();
//        user.clear();
        user.sendKeys(new CharSequence[]{_username});
//        pwd.click();
//        pwd.clear();
        pwd.sendKeys(new CharSequence[]{password});
        submit.click();
        System.out.println("Login test passed");
    }

    public void logoutActions() throws InterruptedException {
        logout.click();
        Thread.sleep(2000);
    }



}
