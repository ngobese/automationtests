package pageObjects.pageObjectsHomePage;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class quotingLitePage {
    final WebDriver driver;

    @FindBy(how = How.XPATH, using = "//div[@class='investec-navbar-toggle-icon']")
    public WebElement menu;

    @FindBy(how = How.XPATH, using = "//a[@id='qot_li']")
    public WebElement goToQuotingLite;

    @FindBy(how = How.XPATH, using = "//span[@class='a-Icon icon-remove']")
    public WebElement exitFilter;

    @FindBy(how = How.XPATH, using = "//input[@id='R21185062502522131_search_field']")
    public WebElement searchQuotes;

    @FindBy(how = How.XPATH, using = "//button[@id='B21185447891522135']")
    public WebElement requestQuote;

    @FindBy(how = How.XPATH, using = "//input[@id='P53_AGREE']")
    public WebElement acceptTerms;

    @FindBy(how = How.XPATH, using = "//input[@id='P53_SUPPLIER_COST']")
    public WebElement supplierCost;

    @FindBy(how = How.XPATH, using = "//input[@id='P53_DUTY_PERCENT']")
    public WebElement dutyPercentage;

    @FindBy(how = How.XPATH, using = "//select[@id='P53_CURRENCY_CODE']")
    public WebElement currency;

    @FindBy(how = How.XPATH, using = "//select[@id='P53_COUNTRY_OF_EXPORT']")
    public WebElement countryOfExport;

    @FindBy(how = How.XPATH, using = "//select[@id='P53_PORT_OF_LOAD']")
    public WebElement port_Of_Load;

    @FindBy(how = How.XPATH, using = "//select[@id='P53_PORT_OF_DISCHARGE']")
    public WebElement port_of_discharg;

    @FindBy(how = How.XPATH, using = "//input[@id='P53_DELIVERY_LOCATION']")
    public WebElement delivery_location;

    @FindBy(how = How.XPATH, using = "//input[@id='P53_SHIPMENT_DATE']")
    public WebElement shipping_date;

    @FindBy(how = How.XPATH, using = "//input[@id='P53_LOAD_TYPE_0']")
    public WebElement selectContainerTypes;

    @FindBy(how = How.XPATH, using = "//input[@id='P53_TOTAL_WEIGHT']")
    public WebElement total_weight;

    @FindBy(how = How.XPATH, using = "//select[@id='P53_40FT']")
    public WebElement containerTypes;

    @FindBy(how = How.XPATH, using = "//select[@id='P53_CUST_PMT_TERM']")
    public WebElement paymentTerms;

    @FindBy(how = How.XPATH, using = "//input[@id='P53_SPOT_RATE']")
    public WebElement spotRate;

    @FindBy(how = How.XPATH, using = "//input[@id='P53_REFERENCE1']")
    public WebElement reference1;

    @FindBy(how = How.XPATH, using = "//input[@id='P53_REFERENCE2']")
    public WebElement reference2;

    @FindBy(how = How.XPATH, using = "//span[@class='t-Button-label'][contains(text(),'Submit Quote Request')]")
    public WebElement submitQuite;

    @FindBy(how = How.XPATH, using = "//dt[@class='t-AVPList-label'][contains(text(),'Port of load')]")
    public WebElement resultsContainPortOfLoad;

    @FindBy(how = How.XPATH, using = "//dt[@class='t-AVPList-label'][contains(text(),'Port of discharge')]")
    public WebElement resultsContainPortOfDischarge;

    @FindBy(how = How.XPATH, using = "//dt[@class='t-AVPList-label'][contains(text(),'Total amount inclusive of VAT')]")
    public WebElement resultsContainTotalAmountInclusiveOfVAT;

    @FindBy(how = How.XPATH, using = "//span[@class='t-Button-label'][contains(text(),'Home')]")
    public WebElement backHome;

    public quotingLitePage(WebDriver driver){
        this.driver = driver;
    }



    public void quotingLiteTests(String supplierForeignCost, String duty, String country, String portOfLoad, String portOfDischarge, String deliveryLocation
            , String totalWeight) throws InterruptedException {

        JavascriptExecutor jse = (JavascriptExecutor)driver;
        menu.click();
        Thread.sleep(1000);
        goToQuotingLite.click();
        Thread.sleep(1000);
        exitFilter.click();
        Thread.sleep(5000);
        searchQuotes.sendKeys(portOfLoad + Keys.ENTER);
        Thread.sleep(1000);
        requestQuote.click();
        Thread.sleep(1000);
        acceptTerms.click();
        supplierCost.sendKeys(supplierForeignCost + Keys.TAB);
        dutyPercentage.sendKeys(duty + Keys.TAB);
        currency.sendKeys(Keys.ARROW_DOWN );
        countryOfExport.sendKeys(country + Keys.ENTER + Keys.TAB);
        Thread.sleep(2000);
        port_Of_Load.sendKeys(portOfLoad + Keys.ENTER + Keys.TAB);
        Thread.sleep(2000);
        port_of_discharg.sendKeys(portOfDischarge + Keys.ENTER + Keys.TAB);
        Thread.sleep(2000);
        delivery_location.sendKeys(deliveryLocation +Keys.ENTER + Keys.TAB);
        Thread.sleep(2000);
        shipping_date.sendKeys("2018-08-30");
        Thread.sleep(2000);
        jse.executeScript("window.scrollBy(0,250)", "");
        selectContainerTypes.click();
        total_weight.sendKeys(totalWeight + Keys.TAB);
        containerTypes.sendKeys(Keys.ARROW_DOWN);
        Thread.sleep(1000);
        paymentTerms.sendKeys(Keys.ARROW_DOWN);
        Thread.sleep(1000);
        spotRate.clear();
        spotRate.sendKeys("12.50");
        Thread.sleep(1000);
        reference1.sendKeys("Auto-test");
        reference2.sendKeys(Keys.TAB);
        submitQuite.click();
        Thread.sleep(5000);
        //Verify Quoting results
        resultsContainPortOfLoad.isDisplayed();
        resultsContainPortOfDischarge.isDisplayed();
        resultsContainTotalAmountInclusiveOfVAT.isDisplayed();
        backHome.click();
        Thread.sleep(3000);
        System.out.println("Quoting Lite test passed");
    }
}
