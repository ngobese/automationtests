package pageObjects.pageObjectsHomePage;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class subscriptionsPage {
    final WebDriver driver;

    @FindBy(how = How.XPATH, using = "//div[@id='investecNavbarToggle']")
    public WebElement menu;

    @FindBy(how = How.XPATH, using = "//a[@id='ao_li']")
    public WebElement activeOrders;

    @FindBy(how = How.XPATH, using = "//span[@class='t-Button-label']")
    public WebElement clickSubscribeButton;

    @FindBy(how = How.CLASS_NAME, using = "text_field apex-item-text")
    public WebElement emailSubject;

    @FindBy(how = How.CLASS_NAME, using = "a-Icon icon-calendar")
    public WebElement dateSelect;

    @FindBy(how = How.CLASS_NAME, using = "datepicker--button")
    public WebElement selectDate;

    @FindBy(how = How.ID, using = "P1000_INTERVAL1")
    public WebElement intervalSelect;

    public subscriptionsPage(WebDriver driver){
        this.driver = driver;
    }

    public void subscribeToReport(String email_Subject) throws InterruptedException {
        menu.click();
        Thread.sleep(1000);
        activeOrders.click();
        Thread.sleep(1000);
        clickSubscribeButton.click();
        emailSubject.sendKeys(email_Subject + Keys.TAB);
        dateSelect.click();
        Thread.sleep(1000);
        selectDate.click();
        Thread.sleep(1000);
        intervalSelect.click();
        intervalSelect.sendKeys(Keys.ARROW_DOWN);
        intervalSelect.sendKeys(Keys.ENTER);
    }
}
