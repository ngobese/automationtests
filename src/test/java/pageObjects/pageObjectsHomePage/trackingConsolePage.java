package pageObjects.pageObjectsHomePage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;


public class trackingConsolePage {

    final WebDriver driver;

    @FindBy(how = How.XPATH, using = ".//*[@id='investecNavbarToggle']/div[3]")
    public WebElement menu;

    @FindBy(how = How.XPATH, using = ".//*[@id='trkli']")
    public WebElement menuItem;

    @FindBy(how = How.XPATH, using = "//button[@id='B37607553744650161']")
    public WebElement followShipment;

    @FindBy(how = How.XPATH, using = "//span[@class='ui-button-icon-primary ui-icon ui-icon-closethick']")
    public WebElement exitFollowScreen;

    public trackingConsolePage(WebDriver driver){

        this.driver = driver;
    }

    public void trackingConsole()  throws InterruptedException   {
        menu.click();
        Thread.sleep(1000);
        menuItem.click();
        Thread.sleep(2000);
        followShipment.click();
//Wait for data to load
        Thread.sleep(5000);
        exitFollowScreen.click();
        Thread.sleep(2000);
        Assert.assertTrue(driver.getPageSource().contains("Movement Plan"));
        Thread.sleep(2000);
        System.out.println("Tracking Console passed");
    }
}

