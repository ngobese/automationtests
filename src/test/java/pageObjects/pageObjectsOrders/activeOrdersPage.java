package pageObjects.pageObjectsOrders;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class activeOrdersPage {

    final WebDriver driver;

    @FindBy(how = How.XPATH, using = ".//*[@id='ActiveOrders_search_field']")
    public WebElement searchForData;

    @FindBy(how = How.CSS, using = "body.t-PageBody.t-PageBody--hideLeft.t-PageBody--hideActions.apex-top-nav.apex-icons-fontawesome.js-HeaderExpanded.t-PageBody--topNav:nth-child(2) div.t-Body:nth-child(9) div.t-Body-main div.t-Body-content div.t-Body-contentInner div.container div.row div.col.col-12 div.t-Region.js-showMaximizeButton.t-Region--removeHeader.t-Region--accent5.t-Region--hiddenOverflow.js-apex-region div.t-Region-bodyWrap div.t-Region-body:nth-child(2) div.a-IRR-container:nth-child(5) div.a-IRR div.a-IRR-fullView div.a-IRR-toolbar div.a-IRR-buttons button.t-Button > span.t-Button-label")
    public WebElement subscribeButton;

    @FindBy(how = How.CSS, using = "//span[@class='t-Button-label']")
    public WebElement exitSubscribeScreen;

    @FindBy(how = How.CSS, using = "body.t-PageBody.t-PageBody--hideLeft.t-PageBody--hideActions.apex-top-nav.apex-icons-fontawesome.js-HeaderExpanded.t-PageBody--topNav:nth-child(2) div.t-Body:nth-child(9) div.t-Body-main div.t-Body-content div.t-Body-contentInner div.container div.row div.col.col-12 div.t-Region.js-showMaximizeButton.t-Region--removeHeader.t-Region--accent5.t-Region--hiddenOverflow.js-apex-region div.t-Region-bodyWrap div.t-Region-body:nth-child(2) div.a-IRR-container:nth-child(5) div.a-IRR div.a-IRR-fullView div.a-IRR-content div.a-IRR-reportView:nth-child(11) div.a-IRR-tableContainer div.t-fht-wrapper div.t-fht-tbody table.a-IRR-table tbody:nth-child(1) tr:nth-child(2) > td.u-tL:nth-child(1)")
    public WebElement verifyIndentColumn;

    @FindBy(how = How.XPATH, using = ".//*[@id='ActiveOrders_data_panel']/div[2]/ul/li[3]/button")
    public WebElement nextPageOfOrders;

    @FindBy(how = How.XPATH, using = ".//*[@id='investecNavbarToggle']/div[3]")
    public WebElement menu;

    @FindBy(how = How.XPATH, using = ".//*[@id='ao_li']")
    public WebElement navigateToActiveOrders;


    public activeOrdersPage(WebDriver driver){
        this.driver = driver;
    }

    public void verifyActiveOrders() throws InterruptedException {
        menu.click();
        Thread.sleep(1000);
        navigateToActiveOrders.click();
        Thread.sleep(1000);
        searchForData.sendKeys("USD" + Keys.ENTER);
        Thread.sleep(1000);
        verifyIndentColumn.isDisplayed();
        System.out.println("Active orders tests passed");
    }
}
