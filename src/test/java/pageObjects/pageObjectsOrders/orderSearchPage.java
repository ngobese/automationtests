package pageObjects.pageObjectsOrders;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class orderSearchPage {

    final WebDriver driver;

    @FindBy(how = How.XPATH, using = ".//*[@id='P21_SEARCH']")
    public WebElement searchText;

    @FindBy(how = How.XPATH, using = "//span[@class='t-Button-label'][contains(text(),'Search')]")
    public WebElement buttonSearch;

    @FindBy(how = How.CSS, using = "body.t-PageBody.t-PageBody--hideLeft.t-PageBody--hideActions.apex-top-nav.apex-icons-fontawesome.js-HeaderExpanded.t-PageBody--topNav:nth-child(2) div.t-Body:nth-child(9) div.t-Body-main div.t-Body-content div.t-Body-contentInner div.container div.row:nth-child(2) div.col.col-12 div.container div.row div.col.col-8:nth-child(2) div.t-Region.js-showMaximizeButton.t-Region--accent5.t-Region--hiddenOverflow div.t-Region-bodyWrap div.t-Region-body:nth-child(2) div.t-SearchResults ul.t-SearchResults-list li.t-SearchResults-item:nth-child(1) h3.t-SearchResults-title > a:nth-child(1)")
    public WebElement selectTheOrder;

    @FindBy(how = How.XPATH, using = "html/body/form/div[1]/div/div[2]/div/div/div[1]/div/div/table/tbody/tr/td/button")
    public WebElement returnToSearch;

    @FindBy(how = How.XPATH, using = "//html//tr[2]/td[2]")
    public WebElement verifyDescriptionExist;

    @FindBy(how = How.XPATH, using = ".//*[@id='investecNavbarToggle']/div[3]")
    public WebElement menu;

    @FindBy(how = How.XPATH, using = ".//*[@id='os_li']")
    public WebElement goToOrderSearch;

    public orderSearchPage(WebDriver driver){
        this.driver = driver;
    }

    public void searchForOrders(String searchTerm, String orderLineDescr) throws InterruptedException {
        menu.click();
        Thread.sleep(1000);
        goToOrderSearch.click();
        Thread.sleep(1000);
        searchText.sendKeys(searchTerm);
        buttonSearch.click();
        Thread.sleep(2000);
        selectTheOrder.click();
        Thread.sleep(2000);
        verifyDescriptionExist.isDisplayed();
        Thread.sleep(1000);
        returnToSearch.click();
        System.out.println("Order search tests passed");
    }
}

