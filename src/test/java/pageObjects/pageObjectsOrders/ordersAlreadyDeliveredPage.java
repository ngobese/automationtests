package pageObjects.pageObjectsOrders;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import sun.awt.windows.ThemeReader;

public class ordersAlreadyDeliveredPage {

    final WebDriver driver;

    @FindBy(how = How.CSS, using = "body.t-PageBody.t-PageBody--hideLeft.t-PageBody--hideActions.apex-top-nav.apex-icons-fontawesome.js-HeaderExpanded.t-PageBody--topNav.pace-done:nth-child(2) div.t-Body:nth-child(9) div.t-Body-main div.t-Body-content div.t-Body-contentInner div.container div.row div.col.col-12 div.t-Region.js-showMaximizeButton.t-Region--accent5.t-Region--hiddenOverflow.js-apex-region div.t-Region-bodyWrap div.t-Region-body:nth-child(2) div.a-IRR-container:nth-child(2) div.a-IRR div.a-IRR-fullView div.a-IRR-content div.a-IRR-reportView:nth-child(10) div.a-IRR-tableContainer div.t-fht-wrapper div.t-fht-tbody table.a-IRR-table tbody:nth-child(1) tr:nth-child(2) > td.u-tL:nth-child(1)")
    public WebElement verifyIndentColumn;

    @FindBy(how = How.XPATH, using = ".//*[@id='C-187209853206914']/a")
    public WebElement verifyDivisionColumn;

    @FindBy(how = How.CSS, using = "body.t-PageBody.t-PageBody--hideLeft.t-PageBody--hideActions.apex-top-nav.apex-icons-fontawesome.js-HeaderExpanded.t-PageBody--topNav.pace-done:nth-child(2) div.t-Body:nth-child(9) div.t-Body-main div.t-Body-content div.t-Body-contentInner div.container div.row div.col.col-12 div.t-Region.js-showMaximizeButton.t-Region--accent5.t-Region--hiddenOverflow.js-apex-region div.t-Region-bodyWrap div.t-Region-body:nth-child(2) div.a-IRR-container:nth-child(2) div.a-IRR div.a-IRR-fullView div.a-IRR-content div.a-IRR-reportView:nth-child(10) div.a-IRR-tableContainer div.t-fht-wrapper div.t-fht-tbody table.a-IRR-table tbody:nth-child(1) tr:nth-child(2) > td.u-tL:nth-child(2)")
    public WebElement verifySupplierName;

    @FindBy(how = How.XPATH, using = ".//*[@id='os_li']")
    public WebElement goToOrderSearch;

    @FindBy(how = How.XPATH, using = ".//*[@id='investecNavbarToggle']/div[3]")
    public WebElement menu;

    @FindBy(how = How.XPATH, using = ".//*[@id='do_li']")
    public WebElement navigateToOrdersAlreadyDelivered;

    public ordersAlreadyDeliveredPage(WebDriver driver){
        this.driver = driver;
    }

    public void verifyOrdersAlreadyDelivered() throws InterruptedException {
        menu.click();
        Thread.sleep(1000);
        navigateToOrdersAlreadyDelivered.click();
        Thread.sleep(1000);
        verifyIndentColumn.isDisplayed();
        verifySupplierName.isDisplayed();
        System.out.println("Orders already delivered tests passed");
    }
}
