package pageObjects.pageObjectsOrders;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class ordersOverview {
    final WebDriver driver;

    @FindBy(how = How.XPATH, using = ".//*[@id='ActiveOrders']/div[2]/div[2]")
    public WebElement verifyPlacedOrders;

    @FindBy(how = How.CSS, using = "body.t-PageBody.t-PageBody--hideLeft.t-PageBody--hideActions.apex-top-nav.apex-icons-fontawesome.js-HeaderExpanded.t-PageBody--topNav.pace-done:nth-child(2) div.t-Body:nth-child(9) div.t-Body-main div.t-Body-content div.t-Body-contentInner div.container div.row:nth-child(1) div.col.col-12 div.container div.row:nth-child(1) div.col.col-12 div.t-Region.t-Region--removeHeader.t-Region--noUI.t-Region--scrollBody div.t-Region-bodyWrap div.t-Region-body:nth-child(2) ul.t-BadgeList.t-BadgeList--circular.t-BadgeList--large.t-BadgeList--fixed li.t-BadgeList-item:nth-child(1) a.t-BadgeList-wrap > span.t-BadgeList-value")
    public WebElement clickOnPlacedOrders;

    @FindBy(how = How.XPATH, using = "//span[@class='t-Button-label'][contains(text(),'Return to Orders Overview')]")
    public WebElement returnToOrdersOverview;

    @FindBy(how = How.XPATH, using = ".//*[@id='R20043120207063735']/div/div[2]/div[1]")
    public WebElement TopOrdersByLandedCost;

    @FindBy(how = How.XPATH, using = ".//*[@id='R20588227670784044']/div[2]/div[2]")
    public WebElement TotalLandedCost;

    @FindBy(how = How.XPATH, using = ".//*[@id='R20969546192347711']/div[2]/div[2]")
    public WebElement TopOrdersByDutyCost;

    @FindBy(how = How.XPATH, using = ".//*[@id='R20969081363347706']/div[2]/div[2]")
    public WebElement SupplierCostAsPartOfLandedCost;

    @FindBy(how = How.XPATH, using = ".//*[@id='ORD05']/div[2]/div[2]")
    public WebElement OrdersByCurrency;

    @FindBy(how = How.XPATH, using = ".//*[@id='ORD01']/div[2]/div[2]")
    public WebElement OrdersDeliveredByMonth;

    @FindBy(how = How.XPATH, using = ".//*[@id='R9977588340948189']/div[2]/div[2]")
    public WebElement AverageFactor;

    @FindBy(how = How.XPATH, using = ".//*[@id='R20971663359347732']/div[2]/div[2]")
    public WebElement Top10ItemsDelivered;

    @FindBy(how = How.XPATH, using = ".//*[@id='investecNavbarToggle']/div[3]")
    public WebElement menu;



    @FindBy(how = How.XPATH, using = ".//*[@id='oo_li']")
    public WebElement goToOrdersOverview;

    public ordersOverview(WebDriver driver){
        this.driver = driver;

            }

            public void verifyOrdersOverview() throws InterruptedException {
                menu.click();
                Thread.sleep(1000);
                goToOrdersOverview.click();
                Thread.sleep(2000);
                Assert.assertTrue(driver.getTitle().contentEquals("Orders Overview"));
                Assert.assertTrue(driver.getPageSource().contains("Ovec Fip Cutdj C W"));
                clickOnPlacedOrders.click();
                Thread.sleep(1000);
                verifyPlacedOrders.isDisplayed();
                Thread.sleep(2000);

                if (returnToOrdersOverview.isDisplayed()) {
                    returnToOrdersOverview.click();
                }
                else {
                    driver.navigate().back();
                }
                Thread.sleep(1000);
                System.out.println("Orders Overview passed");
            }

}
