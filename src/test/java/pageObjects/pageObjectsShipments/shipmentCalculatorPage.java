package pageObjects.pageObjectsShipments;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class shipmentCalculatorPage {

    final WebDriver driver;

    @FindBy(how = How.XPATH, using = ".//*[@id='P28_SHIPMENT_METHOD']")
    public WebElement shipmentMethodDropdown;

    @FindBy(how = How.XPATH, using = ".//*[@id='P28_SHIPMENT_METHOD']/option[3]")
    public WebElement ocean;

    @FindBy(how = How.XPATH, using = ".//*[@id='P28_COUNTRY_OF_EXPORT']")
    public WebElement countryOfExportDropdown;

    @FindBy(how = How.XPATH, using = ".//*[@id='P28_COUNTRY_OF_EXPORT']/option[14]")
    public WebElement selectCountry;

    @FindBy(how = How.XPATH, using = ".//*[@id='P28_PORT_OF_LOAD']")
    public WebElement portOfLoad;

    @FindBy(how = How.XPATH, using = ".//*[@id='P28_PORT_OF_LOAD']/option[2]")
    public WebElement selectPortOfloard;

    @FindBy(how = How.XPATH, using = ".//*[@id='P28_PORT_OF_DISCHARGE']")
    public WebElement portOfDischarge;

    @FindBy(how = How.XPATH, using = ".//*[@id='P28_PORT_OF_DISCHARGE']/option[3]")
    public WebElement selectPortOfDischarge;

    @FindBy(how = How.XPATH, using = ".//*[@id='P28_REQUIRED_DELIVERY_DATE']")
    public WebElement reqDeliveryDate;

    @FindBy(how = How.XPATH, using = "//span[@class='t-Button-label'][contains(text(),'Calculate dates')]")
    public WebElement calculateDates;

    @FindBy(how = How.XPATH, using = "//div[@class='ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix']//button[@type='button']")
    public WebElement closeTheCalc;

    @FindBy(how = How.XPATH, using = "//span[@class='t-Button-label'][contains(text(),'Reset')]")
    public WebElement resetFields;

    @FindBy(how = How.XPATH, using = ".//*[@id='investecNavbarToggle']/div[3]")
    public WebElement menu;

    @FindBy(how = How.XPATH, using = ".//*[@id='shipcalc_li']")
    public WebElement goToShipmentCalc;

    public shipmentCalculatorPage(WebDriver driver){
        this.driver = driver;
    }

    public void calculateShipmentDates(String calcDate) throws InterruptedException {
        JavascriptExecutor jse = (JavascriptExecutor)driver;

        menu.click();
        Thread.sleep(1000);
        goToShipmentCalc.click();
        shipmentMethodDropdown.click();
        ocean.click();
        countryOfExportDropdown.click();
        selectCountry.click();
        portOfLoad.click();
        selectPortOfloard.click();
        portOfDischarge.click();
        selectPortOfDischarge.click();
        reqDeliveryDate.sendKeys(calcDate);
        Thread.sleep(2000);
        jse.executeScript("window.scrollBy(0,250)","");
        Thread.sleep(2000);
        calculateDates.click();
        Thread.sleep(5000);
        closeTheCalc.click();
        Thread.sleep(1000);
        resetFields.click();
        Thread.sleep(1000);
        System.out.println("Shipment calculator test passed");

    }
}
