package pageObjects.pageObjectsShipments;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class shipmentCalendarPage {
    final WebDriver driver;

    @FindBy(how = How.XPATH, using = ".//*[@id='shipcal_li']")
    public WebElement goToShipmentCalendar;

    @FindBy(how = How.XPATH, using = ".//*[@id='investecNavbarToggle']/div[3]")
    public WebElement menu;

    @FindBy(how = How.XPATH, using = ".//*[@id='P29_CALENDAR']")
    public WebElement calendarDropdown;

    @FindBy(how = How.XPATH, using = "//*[@id='P29_CALENDAR']/option[2]")
    public WebElement allShipments;

    @FindBy(how = How.XPATH, using = ".//*[@id='R33854766660173222']/div[2]/div[2]")
    public WebElement siteCalendar;

    @FindBy(how = How.XPATH, using = "//span[@class='ui-icon ui-icon-circle-triangle-e']")
    public WebElement nextMonth;

    @FindBy(how = How.XPATH, using = "//button[@type='button'][contains(text(),'list')]")
    public WebElement changeCalendarView;

    public shipmentCalendarPage(WebDriver driver){
        this.driver = driver;
    }

    public void verifyShipmentCalendar() throws InterruptedException {
        menu.click();
        Thread.sleep(1000);
        goToShipmentCalendar.click();
        Thread.sleep(1000);
        calendarDropdown.click();
        allShipments.click();
        Thread.sleep(2000);
       // siteCalendar.isDisplayed();
        nextMonth.click();
        Thread.sleep(2000);
        changeCalendarView.click();
        Thread.sleep(3000);
        System.out.println("Shipment calender test passed");
    }
}
