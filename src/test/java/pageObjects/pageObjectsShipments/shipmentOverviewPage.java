package pageObjects.pageObjectsShipments;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class shipmentOverviewPage {

    final WebDriver driver;

    @FindBy(how = How.XPATH, using = "//span[@class='t-BadgeList-label'][contains(text(),'Ship on board next 7 days')]")
    public WebElement shipOnBoardNext7Days;

    @FindBy(how = How.XPATH, using = "//html//tr[2]/td[2]")
    public WebElement shipmentNumberColumn;

    @FindBy(how = How.CSS, using = "body.t-PageBody.t-PageBody--hideLeft.t-PageBody--hideActions.apex-top-nav.apex-icons-fontawesome.js-HeaderExpanded.t-PageBody--topNav:nth-child(2) div.t-Body:nth-child(9) div.t-Body-main div.t-Body-content div.t-Body-contentInner div.container div.row:nth-child(1) div.col.col-12 div.t-Region.js-showMaximizeButton.t-Region--accent5.t-Region--hiddenOverflow.js-apex-region div.t-Region-bodyWrap div.t-Region-body:nth-child(2) div.a-IRR-container:nth-child(5) div.a-IRR div.a-IRR-fullView div.a-IRR-toolbar div.a-IRR-buttons button.t-Button.t-Button--icon.js-ignoreChange.t-Button--iconRight:nth-child(2) > span.t-Button-label:nth-child(2)")
    public WebElement returnToShipmentsOverview;

    @FindBy(how = How.XPATH, using = "//span[@class='t-BadgeList-label'][contains(text(),'In transit shipments')]")
    public WebElement inTransitShipments;

    @FindBy(how = How.CSS, using = "body.t-PageBody.t-PageBody--hideLeft.t-PageBody--hideActions.apex-top-nav.apex-icons-fontawesome.js-HeaderExpanded.t-PageBody--topNav:nth-child(2) div.t-Body:nth-child(9) div.t-Body-main div.t-Body-content div.t-Body-contentInner div.container div.row:nth-child(1) div.col.col-12 div.t-Region.js-showMaximizeButton.t-Region--accent5.t-Region--hiddenOverflow.js-apex-region div.t-Region-bodyWrap div.t-Region-body:nth-child(2) div.a-IRR-container:nth-child(5) div.a-IRR div.a-IRR-fullView div.a-IRR-content div.a-IRR-reportView:nth-child(11) div.a-IRR-tableContainer div.t-fht-wrapper div.t-fht-tbody:nth-child(3) table.a-IRR-table tbody:nth-child(1) tr:nth-child(2) td.u-tL:nth-child(2) > span:nth-child(1)")
    public WebElement indentNumberColumn;

    @FindBy(how = How.CSS, using = "body.t-PageBody.t-PageBody--hideLeft.t-PageBody--hideActions.apex-top-nav.apex-icons-fontawesome.js-HeaderExpanded.t-PageBody--topNav:nth-child(2) div.t-Body:nth-child(9) div.t-Body-main div.t-Body-content div.t-Body-contentInner div.container div.row:nth-child(1) div.col.col-12 div.t-Region.js-showMaximizeButton.t-Region--accent5.t-Region--hiddenOverflow.js-apex-region div.t-Region-bodyWrap div.t-Region-body:nth-child(2) div.a-IRR-container:nth-child(5) div.a-IRR div.a-IRR-fullView div.a-IRR-content div.a-IRR-reportView:nth-child(11) div.a-IRR-tableContainer div.t-fht-wrapper div.t-fht-tbody:nth-child(3) table.a-IRR-table tbody:nth-child(1) tr:nth-child(2) > td.u-tL:nth-child(3)")
    public WebElement supplierNameColumn;

    @FindBy(how = How.XPATH, using = "//span[@class='t-BadgeList-label'][contains(text(),'Arriving next 14 days')]")
    public WebElement arrivingNext14Days;

    @FindBy(how = How.CSS, using = "body.t-PageBody.t-PageBody--hideLeft.t-PageBody--hideActions.apex-top-nav.apex-icons-fontawesome.t-PageBody--topNav.js-HeaderExpanded:nth-child(2) div.t-Body:nth-child(9) div.t-Body-main div.t-Body-content div.t-Body-contentInner div.container div.row:nth-child(1) div.col.col-12 div.t-Region.js-showMaximizeButton.t-Region--accent5.t-Region--hiddenOverflow.js-apex-region div.t-Region-bodyWrap div.t-Region-body:nth-child(2) div.a-IRR-container:nth-child(5) div.a-IRR div.a-IRR-fullView div.a-IRR-content div.a-IRR-reportView:nth-child(11) div.a-IRR-tableContainer div.t-fht-wrapper div.t-fht-tbody:nth-child(3) table.a-IRR-table tbody:nth-child(1) tr:nth-child(2) > td.u-tL:nth-child(4)")
    public WebElement verifyPortOfDischarge;

    @FindBy(how = How.XPATH, using = "//html//li[1]/a[1]/span[3]")
    public WebElement verifyHighestValueShipments;

    @FindBy(how = How.XPATH, using = "//html//li[1]/span[1]")
    public WebElement verifyShipmentsPerMonth;

    @FindBy(how = How.XPATH, using = "//div[@id='R281775355837478313']")
    public WebElement verifyOceanContainerUse;

    @FindBy(how = How.CSS, using = "body.t-PageBody.t-PageBody--hideLeft.t-PageBody--hideActions.apex-top-nav.apex-icons-fontawesome.t-PageBody--topNav.js-HeaderExpanded:nth-child(2) div.t-Body:nth-child(9) div.t-Body-main div.t-Body-content div.t-Body-contentInner div.container div.row:nth-child(4) div.col.col-12 div.container div.row div.col.col-6:nth-child(1) div.t-Region.dualChart.js-showMaximizeButton.t-Region--accent5.t-Region--scrollBody.js-apex-region div.t-Region-bodyWrap div.t-Region-body:nth-child(2) div.oj-dvtbase.oj-chart.oj-component-initnode g:nth-child(1) g:nth-child(5) g:nth-child(11) g:nth-child(12) > polygon:nth-child(3)")
    public WebElement verifyLandedCostByCurrency;

    @FindBy(how = How.CSS, using = "body.t-PageBody.t-PageBody--hideLeft.t-PageBody--hideActions.apex-top-nav.apex-icons-fontawesome.t-PageBody--topNav.js-HeaderExpanded:nth-child(2) div.t-Body:nth-child(9) div.t-Body-main div.t-Body-content div.t-Body-contentInner div.container div.row:nth-child(4) div.col.col-12 div.container div.row div.col.col-6:nth-child(2) div.t-Region.js-showMaximizeButton.t-Region--accent5.t-Region--scrollBody div.t-Region-bodyWrap div.t-Region-body:nth-child(2) div.t-Report.t-Report--altRowsDefault.t-Report--rowHighlight div.t-Report-wrap div.t-Report-tableWrap:nth-child(2) table.t-Report-report tbody:nth-child(2) tr:nth-child(1) > td.t-Report-cell:nth-child(3)")
    public WebElement verifyAverageTransitTime;

    @FindBy(how = How.XPATH, using = ".//*[@id='investecNavbarToggle']/div[3]")
    public WebElement menu;

    @FindBy(how = How.XPATH, using = ".//*[@id='shipo_li']")
    public WebElement goToShipmentOverview;

    public shipmentOverviewPage(WebDriver driver){
        this.driver = driver;
    }

    public void verifyShipmentOverview() throws InterruptedException {
        menu.click();
        Thread.sleep(1000);
        goToShipmentOverview.click();
        Thread.sleep(1000);
        Assert.assertTrue(verifyHighestValueShipments.isDisplayed());
        Assert.assertTrue(verifyShipmentsPerMonth.isDisplayed());
        shipOnBoardNext7Days.click();
        Thread.sleep(2000);
        Assert.assertTrue(shipmentNumberColumn.isDisplayed());
        Thread.sleep(2000);
        returnToShipmentsOverview.click();
        Thread.sleep(3000);
        inTransitShipments.click();
        Thread.sleep(3000);
        Assert.assertTrue(indentNumberColumn.isDisplayed());
        Assert.assertTrue(supplierNameColumn.isDisplayed());
        returnToShipmentsOverview.click();
        Thread.sleep(3000);
        arrivingNext14Days.click();
        Thread.sleep(3000);
        Assert.assertTrue(verifyPortOfDischarge.isDisplayed());
        returnToShipmentsOverview.click();
        Thread.sleep(3000);
        System.out.println("Shipment overview test passed");

    }
}