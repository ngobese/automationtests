package pageObjects.pageObjectsShipments;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class shipmentSearchPage {

    final WebDriver driver;

    @FindBy(how = How.XPATH, using = ".//*[@id='investecNavbarToggle']/div[3]")
    public WebElement menu;

    @FindBy(how = How.XPATH, using = ".//*[@id='shipsch_li']")
    public WebElement goToShipment;

    @FindBy(how = How.XPATH, using = ".//*[@id='P31_SEARCH']")
    public WebElement searchShipments;

    @FindBy(how = How.LINK_TEXT, using = "DS175222")
    public WebElement selectShipment;

    @FindBy(how = How.XPATH, using = "//span[@class='searchResults-title-lg']")
    public WebElement verifyShipmentNumber;

    @FindBy(how = How.XPATH, using = "//dd[@class='t-AVPList-value'][contains(text(),'1,608.00')]")
    public WebElement verifyTotalOrderValue;

    @FindBy(how = How.XPATH, using = "//th[@id='C3772318680773073']//a[@class='a-IRR-headerLink']")
    public WebElement verifyIndentNumber;

    @FindBy(how = How.XPATH, using = "//span[@class='t-Button-label']")
    public WebElement returnToSearch;

    public shipmentSearchPage(WebDriver driver){
        this.driver = driver;
    }

    public void searchForShipments(String Supplier) throws InterruptedException {
        menu.click();
        Thread.sleep(1000);
        goToShipment.click();
        searchShipments.sendKeys(Supplier + Keys.ENTER);
        Thread.sleep(1000);
        selectShipment.click();
        verifyShipmentNumber.isDisplayed();
        verifyTotalOrderValue.isDisplayed();
//        verifyIndentNumber.isDisplayed();
        returnToSearch.click();
        Thread.sleep(2000);
        System.out.println("Shipment search tests passed");
    }
}
