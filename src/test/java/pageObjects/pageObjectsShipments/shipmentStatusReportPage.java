package pageObjects.pageObjectsShipments;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class shipmentStatusReportPage {

    final WebDriver driver;

    @FindBy(how = How.XPATH, using = ".//*[@id='investecNavbarToggle']/div[3]")
    public WebElement menu;

    @FindBy(how = How.XPATH, using = ".//*[@id='shipstat_li']")
    public WebElement goToShipmentStatus;

    @FindBy(how = How.XPATH, using = ".//*[@id='shipmentStatus_search_field']")
    public WebElement searchShipments;

    @FindBy(how = How.CLASS_NAME, using = " u-tL")
    public WebElement shipmentNumberColumn;

    @FindBy(how = How.XPATH, using = ".//html//li[1]/span[4]/button[1]/span[1]")
    public WebElement closeFilter1;


    public shipmentStatusReportPage(WebDriver driver){

        this.driver = driver;
    }

    public void verifyShipmentStatusReport() throws InterruptedException {
        menu.click();
        Thread.sleep(1000);
        goToShipmentStatus.click();
            Thread.sleep(2000);
            searchShipments.sendKeys("Jul-2018" + Keys.ENTER);
            Thread.sleep(2000);
            shipmentNumberColumn.isDisplayed();
        System.out.println("Shipment status report tests passed");
    }
}
