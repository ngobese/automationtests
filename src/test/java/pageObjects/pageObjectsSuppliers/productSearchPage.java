package pageObjects.pageObjectsSuppliers;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class productSearchPage {

    final WebDriver driver;

    @FindBy(how = How.XPATH, using = ".//*[@id='investecNavbarToggle']/div[3]")
    public WebElement menu;

    @FindBy(how = How.XPATH, using = "//a[@id='prdsch_li']")
    public WebElement goToProductSearch;

    @FindBy(how = How.XPATH, using = "//input[@id='P38_SEARCH']")
    public WebElement searchByItem;

    @FindBy(how = How.LINK_TEXT, using = "Ck16ee2282284211 Qepene Qsu 6 C4211 Qiesm")
    public WebElement clickSearchResults;

    @FindBy(how = How.XPATH, using = "//html//div[contains(@class,'col col-11')]//li[6]/a[1]/span[1]")
    public WebElement verifyProductSummary;

    @FindBy(how = How.XPATH, using = "//html//div[@id='R379935854373278424']//div[@class='t-Report-tableWrap']//td[1]")
    public WebElement verifyOrdersAlreadyDelivered;

    public productSearchPage(WebDriver driver){
        this.driver = driver;
    }

    public void productSearch(String itemReference) throws InterruptedException {

        menu.click();
        Thread.sleep(1000);
        goToProductSearch.click();
        Thread.sleep(2000);
        searchByItem.sendKeys(itemReference + Keys.ENTER);
        Thread.sleep(2000);
        clickSearchResults.click();
        Thread.sleep(2000);
        verifyProductSummary.isDisplayed();
        verifyOrdersAlreadyDelivered.isDisplayed();
        System.out.println("Product search tests passed");

    }

}