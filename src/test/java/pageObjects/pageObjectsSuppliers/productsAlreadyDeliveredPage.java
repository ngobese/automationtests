package pageObjects.pageObjectsSuppliers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class productsAlreadyDeliveredPage {

    final WebDriver driver;

    @FindBy(how = How.XPATH, using = "//div[@id='investecNavbarToggle']")
    public WebElement menu;

    @FindBy(how = How.XPATH, using = "//a[@id='prdad_li']")
    public WebElement goToProdAlreadyDeliv;

    @FindBy(how = How.XPATH, using = "//input[@id='productsDelivered_search_field']")
    public WebElement searchItem;

    @FindBy(how = How.LINK_TEXT, using = "Jf442")
    public WebElement selectIndent;

    @FindBy(how = How.XPATH, using = "//html//tr[2]/td[3]")
    public WebElement verifyItemRef;

    public productsAlreadyDeliveredPage(WebDriver driver){
        this.driver = driver;
    }

    public void productAlreadyDelivered(String itemRef) throws InterruptedException {
        menu.click();
        Thread.sleep(1000);
        goToProdAlreadyDeliv.click();
        Thread.sleep(2000);
        searchItem.sendKeys(itemRef);
        Thread.sleep(2000);
        selectIndent.click();
        Thread.sleep(2000);
        verifyItemRef.isDisplayed();
        System.out.println("Products already delivered tests passed");
    }
}
