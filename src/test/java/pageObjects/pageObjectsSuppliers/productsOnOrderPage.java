package pageObjects.pageObjectsSuppliers;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class productsOnOrderPage {

    final WebDriver driver;

    @FindBy(how = How.XPATH, using = ".//*[@id='investecNavbarToggle']/div[3]")
    public WebElement menu;

    @FindBy(how = How.XPATH, using = ".//*[@id='prdoo_li']")
    public WebElement goToProductsOnOrders;

    @FindBy(how = How.XPATH, using = "//input[@id='productsOnOrder_search_field']")
    public WebElement searchIndentNumber;

    @FindBy(how = How.XPATH, using = "//html//tr[2]/td[4]")
    public WebElement verifyItemReference;

    public productsOnOrderPage(WebDriver driver){
        this.driver = driver;
    }

    public void verifyProductsOoOrder(String indent) throws InterruptedException {
        menu.click();
        Thread.sleep(1000);
        goToProductsOnOrders.click();
        Thread.sleep(1000);
        searchIndentNumber.sendKeys(indent + Keys.ENTER);
        Thread.sleep(2000);
        verifyItemReference.isDisplayed();
        System.out.println("Products on order tests passed");
    }
}
