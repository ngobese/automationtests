package pageObjects.pageObjectsSuppliers;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class supplierDetailsPage {
    final WebDriver driver;

    @FindBy(how = How.XPATH, using = ".//*[@id='investecNavbarToggle']/div[3]")
    public WebElement menu;

    @FindBy(how = How.XPATH, using = ".//*[@id='supd_li']")
    public WebElement goToSupplierDetails;

    @FindBy(how = How.XPATH, using = "//select[@id='P33_SELECT_SUPPLIER']")
    public WebElement supplierDropdown;

    @FindBy(how = How.XPATH, using = "//option[@value='Ceaes Qmetv Aeqo Ysypmiso Tep Wi Vod. Ltd.Tvo.']")
    public WebElement selectASupplier;

    @FindBy(how = How.XPATH, using = "//span[@class='t-Button-label']")
    public WebElement go;

    @FindBy(how = How.XPATH, using = "//span[@class='t-BadgeList-label'][contains(text(),'Orders Already Delivered')]")
    public WebElement ordersAreadyDelivered;

    @FindBy(how = How.CLASS_NAME, using = " u-tL")
    public WebElement verifyOrdersDataExist;

    @FindBy(how = How.XPATH, using = "//span[@class='t-Button-label']")
    public WebElement returnToSupplierDetails;

    @FindBy(how = How.XPATH, using = "//html//div[@id='R65631269840717655']//tr[2]/td[2]")
    public WebElement verifySupplierSummary;

    @FindBy(how = How.XPATH, using = "//html//tr[1]/td[1]")
    public WebElement verifyRecentPayments;

    @FindBy(how = How.XPATH, using = "//span[@class='t-Icon fa fa-bars']")
    public WebElement changeGraphView;

    @FindBy(how = How.ID, using = "supplierSpend_jet")
    public WebElement verifySupplierSpend;

    @FindBy(how = How.ID, using = "R65715466150699949_jet")
    public WebElement verifyDeliveredOrders;

    public supplierDetailsPage(WebDriver driver){
        this.driver = driver;
    }

    public void verifySupplierDetails() throws InterruptedException {
        JavascriptExecutor jse = (JavascriptExecutor)driver;

        menu.click();
        Thread.sleep(1000);
        goToSupplierDetails.click();
        supplierDropdown.click();
        selectASupplier.click();
        go.click();
        Thread.sleep(2000);
        ordersAreadyDelivered.click();
        Thread.sleep(1000);
        verifyOrdersDataExist.isDisplayed();
        returnToSupplierDetails.click();
        Thread.sleep(1000);
        jse.executeScript("window.scrollBy(0,250)","");
        verifySupplierSummary.isDisplayed();
        verifyRecentPayments.isDisplayed();
        jse.executeScript("window.scrollBy(0,250)", "");
        Thread.sleep(2000);
        changeGraphView.click();
        verifySupplierSpend.isDisplayed();
        jse.executeScript("window.scrollBy(0,250)", "");
        Assert.assertTrue(verifyDeliveredOrders.isDisplayed());
        Thread.sleep(2000);
        System.out.println("supplier details tests passed");
    }
}
