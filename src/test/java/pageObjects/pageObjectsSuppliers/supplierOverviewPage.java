package pageObjects.pageObjectsSuppliers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class supplierOverviewPage {
    final WebDriver driver;

    @FindBy(how = How.XPATH, using = ".//*[@id='investecNavbarToggle']/div[3]")
    public WebElement menu;

    @FindBy(how = How.XPATH, using = ".//*[@id='supo_li']")
    public WebElement goToSupplierOverview;

    @FindBy(how = How.XPATH, using = "//html//div[@id='R83064277090875298']//li[1]/a[1]/span[2]")
    public WebElement verifyTopSuppliersByOrdersCount;

    @FindBy(how = How.XPATH, using = "//span[@id='R65629715429717639_a-DetailedContentList-item_00']")
    public WebElement verifyTopSuppliersBySupplierCost;

    @FindBy(how = How.XPATH, using = ".//*[@id='R63205208670609868_a-DetailedContentList-item_00']")
    public WebElement viewDetails;

    @FindBy(how = How.XPATH, using = ".//*[@id='R45770647009452209_slideTooltip']/ul/li[2]/a/span[2]")
    public WebElement viewDetailsByCost;

    @FindBy(how = How.ID, using = "_dvtActiveElement403301697")
    public WebElement verifySupplierCostByYear;

    public supplierOverviewPage(WebDriver driver){
        this.driver = driver;
    }

    public void verifySupplierOverview() throws InterruptedException {

        menu.click();
        Thread.sleep(1000);
        goToSupplierOverview.click();
        Thread.sleep(1000);
        verifyTopSuppliersByOrdersCount.isDisplayed();
        verifyTopSuppliersBySupplierCost.isDisplayed();
        Thread.sleep(1000);
        System.out.println("Supplier overview tests passed");
    }
}
