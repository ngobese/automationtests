package pageObjects.pageObjectsSuppliers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class supplierPerformancePage {
    final WebDriver driver;

    @FindBy(how = How.XPATH, using = ".//*[@id='investecNavbarToggle']/div[3]")
    public WebElement menu;

    @FindBy(how = How.XPATH, using = ".//*[@id='suppf_li']")
    public WebElement goToSupplierPerf;

    @FindBy(how = How.XPATH, using = "//html//tr[2]/td[2]")
    public WebElement AverageLeadTime;

    @FindBy(how = How.XPATH, using = "//select[@id='reportFixed_row_select']")
    public WebElement reportSize;

    @FindBy(how = How.XPATH, using = "//option[@value='5']")
    public WebElement selectSize;

    @FindBy(how = How.XPATH, using = "//span[@class='a-Icon icon-right-chevron']")
    public WebElement nextPage;

    public supplierPerformancePage(WebDriver driver){
        this.driver = driver;
    }

    public void verifySupplierPerformance() throws InterruptedException {
        menu.click();
        Thread.sleep(1000);
        goToSupplierPerf.click();
        Thread.sleep(2000);
        reportSize.click();
        Thread.sleep(1000);
        selectSize.click();
        Thread.sleep(2000);
        nextPage.click();
        Assert.assertTrue(AverageLeadTime.isDisplayed());
        System.out.println("Supplier performance tests passed");
    }
}
