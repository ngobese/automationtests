package pageObjects.payments;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class allPaymentsPage {
    final WebDriver driver;

    @FindBy(how = How.XPATH, using = ".//*[@id='investecNavbarToggle']/div[3]")
    public WebElement menu;

    @FindBy(how = How.XPATH, using = "//a[@id='paya_li']")
    public WebElement goToAllPayments;

    @FindBy(how = How.XPATH, using = "//input[@id='reportFixed_search_field']")
    public WebElement searchPaymentStatus;

    @FindBy(how = How.XPATH, using = "//html//tr[2]/td[2]")
    public WebElement verifyDataExists;

    public allPaymentsPage(WebDriver driver){
        this.driver = driver;
    }

    public void allPaymentsTests(String status) throws InterruptedException {
        menu.click();
        Thread.sleep(1000);
        goToAllPayments.click();
        Thread.sleep(1000);
        searchPaymentStatus.sendKeys(status + Keys.ENTER);
        Thread.sleep(2000);
        verifyDataExists.isDisplayed();
        System.out.println("All payments tests passed");
    }
}
