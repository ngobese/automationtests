package pageObjects.payments;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class paymentsCalendarPage {
    final WebDriver driver;

    @FindBy(how = How.XPATH, using = ".//*[@id='investecNavbarToggle']/div[3]")
    public WebElement menu;

    @FindBy(how = How.XPATH, using = "//a[@id='paycal_li']")
    public WebElement goToPaymentsCaleder;

    @FindBy(how = How.XPATH, using = "//span[@class='ui-icon ui-icon-circle-triangle-w']")
    public WebElement previousMonth;

    @FindBy(how = How.XPATH, using = "//html//div[1]/div[2]/table[1]/tbody[1]/tr[1]/td[5]/a[1]/div[1]")
    public WebElement prevousMonthData;

    @FindBy(how = How.XPATH, using = "//button[@type='button'][contains(text(),'today')]")
    public WebElement currentMonth;

    @FindBy(how = How.XPATH, using = "//html//div[2]/div[2]/table[1]/tbody[1]/tr[1]/td[2]/a[1]/div[1]/span[1]")
    public WebElement currentMonthData;

    @FindBy(how = How.XPATH, using = "//span[@class='ui-icon ui-icon-circle-triangle-e']")
    public WebElement nextMonth;

    @FindBy(how = How.XPATH, using = "//html//div[2]/div[2]/table[1]/tbody[1]/tr[1]/td[2]/a[1]/div[1]")
    public WebElement nextMonthData;

    @FindBy(how = How.XPATH, using = "//button[@type='button'][contains(text(),'list')]")
    public WebElement listView;

    public paymentsCalendarPage(WebDriver driver){
        this.driver = driver;
    }

    public void paymentsCalenderTests() throws InterruptedException {
        menu.click();
        Thread.sleep(1000);
        goToPaymentsCaleder.click();
        Thread.sleep(1000);
        previousMonth.click();
        Thread.sleep(1000);
        currentMonth.click();
        Thread.sleep(1000);
        nextMonth.click();
        Thread.sleep(1000);
        listView.click();
        Thread.sleep(2000);
        System.out.println("Payments Calender tests passed");
    }
}
