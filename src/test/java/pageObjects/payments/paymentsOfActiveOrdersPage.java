package pageObjects.payments;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class paymentsOfActiveOrdersPage {
    final WebDriver driver;

    @FindBy(how = How.XPATH, using = ".//*[@id='investecNavbarToggle']/div[3]")
    public WebElement menu;

    @FindBy(how = How.XPATH, using = "//div[@id='pay_hdg']//div[@class='sub-menu']//div[@class='sub-menu-content']//ul[@class='nav navbar-nav']//li//a[@id='do_li']")
    public WebElement goToPaymentsOfActiveOrders;

    @FindBy(how = How.XPATH, using = "//html//form[@id='wwvFlowForm']//tr[2]/td[1]")
    public WebElement verifyPaymentsNext21days;

    @FindBy(how = How.XPATH, using = "//html//tr[2]/td[1]")
    public WebElement verifyIndent;

    @FindBy(how = How.XPATH, using = "//input[@id='R20768647600904257_search_field']")
    public WebElement searchPaymentTerm;

    public paymentsOfActiveOrdersPage(WebDriver driver){
        this.driver = driver;
    }

    public void paymentsActiveOrders(String paymentTerm) throws InterruptedException {
        menu.click();
        Thread.sleep(1000);
        goToPaymentsOfActiveOrders.click();
        Thread.sleep(1000);
        searchPaymentTerm.sendKeys(paymentTerm + Keys.ENTER);
        Thread.sleep(2000);
        verifyIndent.isDisplayed();
        verifyPaymentsNext21days.isDisplayed();
        System.out.println("Payments of Active orders tests passed");
    }
}
