package pageObjects.payments;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;
import sun.awt.windows.ThemeReader;

public class paymentsOverviewPage {
    final WebDriver driver;

    @FindBy(how = How.XPATH, using = ".//*[@id='investecNavbarToggle']/div[3]")
    public WebElement menu;

    @FindBy(how = How.XPATH, using = "//a[@id='payo_li']")
    public WebElement goToPaymentsOverview;

    @FindBy(how = How.XPATH, using = "//html//form[@id='wwvFlowForm']//tr[2]/td[1]")
    public WebElement verifyPaymentsNext21days;

    @FindBy(how = How.XPATH, using = "//html//label[2]")
    public WebElement viewEuroPayments;

    public paymentsOverviewPage(WebDriver driver){
        this.driver = driver;
    }

    public void paymentsOverviewTests() throws InterruptedException {
        menu.click();
        Thread.sleep(1000);
        goToPaymentsOverview.click();
        Thread.sleep(2000);
        viewEuroPayments.click();
        Thread.sleep(2000);
        System.out.println("Payments overview tests passed");
    }

}
